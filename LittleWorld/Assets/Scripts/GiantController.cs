﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine;

public class GiantController : MonoBehaviour {

	public float speed;
	GameObject[] self;

	void Start(){
		//self = this.GetComponents<GameObject> ();
	}

	void FixedUpdate (){
		Vector3 movement;
		float horizontal, vertical, step;

		// Read input
		horizontal = Input.GetAxis("Horizontal");
		vertical = Input.GetAxis("Vertical");

		Debug.Log (horizontal + " " + vertical);

		step = speed * Time.deltaTime;

		movement = new Vector3(0, 0, -vertical);
		transform.position = Vector3.MoveTowards(transform.position, transform.position + movement, step);
	
	}
}
