﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleManager : MonoBehaviour
{
    public PlatformContentManager platformManager;
    public float minSpeedFactor = 10.0f;
    public float maxSpeedFactor = 25.0f;
    public float speedFactor = 0.0f;

    public float avoidColDistance = 2.0f;
    public float giantColDistance = 0.1f;

    public bool side;

    public bool isBroken = false;

    GameController gameController = null;

    // Use this for initialization
    void Start () {
        speedFactor = Random.Range(minSpeedFactor, maxSpeedFactor);

        //side = Random.Range(0.0f, 1.0f) < 0.5;

        side = false;

        if (!side)
        {
            //Debug.Log("Turned around vehicle");
            transform.Rotate(0.0f, 180.0f, 0.0f);
        }

        gameController = GameObject.Find("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update() {
        if (isBroken)
            return;

        float speed = -1.0f;

        // Collision direction
        RaycastHit hitInfo;

        Vector3 curVelDir;
        if (side)
        {
            curVelDir = new Vector3(1, 0, 0);
        }
        else
        {
            curVelDir = new Vector3(-1, 0, 0);
        }

        //if (Physics.Raycast(new Ray(transform.position, curVelDir), out hitInfo))
        //{
        //    //if (hitInfo.collider.gameObject.CompareTag("Obstacle") && hitInfo.distance < avoidColDistance)
        //    //{
        //    //    VehicleManager other = hitInfo.collider.gameObject.GetComponent<VehicleManager>();
        //    //    if (other != null)
        //    //    {
        //    //        this.platformManager.collideVehicles(this, other);
        //    //    }
        //    //}

        //    if (hitInfo.collider.gameObject.CompareTag("Giant") && hitInfo.distance < giantColDistance)
        //    {
        //        this.platformManager.killVehicle(this);
        //    }
        //}

        if (side)
        {
            //Debug.Log("Velocity modifier: " + - speedFactor * speed);
            this.transform.position -= new Vector3(speedFactor*speed, 0, 0) * Time.fixedDeltaTime;
        }
        else
        {
            //Debug.Log("Velocity modifier: " + speedFactor * speed);
            this.transform.position += new Vector3(speedFactor*speed, 0, 0) * Time.fixedDeltaTime;
        }
	}

    public void OnTriggerStay(Collider collider)
    {
		if (this.isBroken)
            return;

        //if (collider.gameObject.CompareTag("Obstacle"))
        //{
        //    VehicleManager other = collider.gameObject.GetComponent<VehicleManager>();

        //    this.platformManager.collideVehicles(this, other);
        //}

        if (collider.gameObject.CompareTag("Giant"))
        {

            if (collider.gameObject.name.CompareTo("FootProxy") != 0)
                return;

            ShadowControl shadowControl = collider.gameObject.transform.parent.gameObject.GetComponent<ShadowControl>();
            if (shadowControl != null && shadowControl.ground)
            {
                this.platformManager.killVehicle(this);
            }
        }
    }

}
