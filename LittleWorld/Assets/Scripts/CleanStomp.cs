﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanStomp : MonoBehaviour {

    public bool clean = true;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void LateUpdate () {
        clean = true;
    }

    void OnTriggerEnter(Collider c)
    {
        clean = false;
    }

    void OnTriggerStay(Collider c)
    {
        clean = false;
    }
}
