﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragControl : MonoBehaviour {

    Vector3 oldPointer;
    bool dragging = false;
    public float vel;

    public LayerMask lm_shadow;
    public LayerMask lm_floor;

    public GameObject leftShadow;
    public GameObject rightShadow;

    public ParticleSystem leftParticles;
    public ParticleSystem rightParticles;

    public GameObject leftClean;
    public GameObject rightClean;

    public GameObject platform;

    public GameController gameController;

    public GameObject animatedLeft;
    public GameObject animatedRight;

    public AudioClip stompAudio;

    bool leftTurn = true;

    private float leftIniPos;
    private float rightIniPos;
    private bool firstTime = true;

    bool cameraShaking=false;

    // Use this for initialization
    void Start () {
        ((ShadowControl)leftShadow.GetComponent("ShadowControl")).active = false;
        ((ShadowControl)rightShadow.GetComponent("ShadowControl")).active = false;

        ((ShadowControl)leftShadow.GetComponent("ShadowControl")).ChangeTurn();
        ((ShadowControl)rightShadow.GetComponent("ShadowControl")).ChangeTurn();

        ((ShadowControl)leftShadow.GetComponent("ShadowControl")).ground = true;
        ((ShadowControl)rightShadow.GetComponent("ShadowControl")).ground = true;

        //leftTurn = true;
        //firstTime = true;
        gameController.balancing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!dragging)
        {
            //Debug.Log("NOT DRAGGING");
            if (Input.GetMouseButtonDown(0))
            {
                //oldPointer = Input.mousePosition;

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, lm_shadow.value))
                {
                    if (((ShadowControl)hit.collider.gameObject.GetComponent("ShadowControl")).active)
                    {

                        //Debug.Log("CLIC IN SHADOW");
                        //Touch floor
                        oldPointer = hit.point;
                        dragging = true;


                        //HIDE AND STOP SHADOW
                        //hit.rigidbody.gameObject.GetComponent<Projector>().enabled = false;
                        ((PlatformContentManager)platform.GetComponent("PlatformContentManager")).worldVelocity = 0;

                        if(firstTime)
                        {
                            leftTurn = (hit.collider.transform.position.z > 0);
                            if(leftTurn)
                                ((ShadowControl)rightShadow.GetComponent("ShadowControl")).ChangeTurn();
                            else
                                ((ShadowControl)leftShadow.GetComponent("ShadowControl")).ChangeTurn();
                            gameController.balancing = true;
                            firstTime = false;
                        }
                        //LEG TURN SWITCH
                        ((ShadowControl)leftShadow.GetComponent("ShadowControl")).ChangeTurn();
                        ((ShadowControl)rightShadow.GetComponent("ShadowControl")).ChangeTurn();
                        leftTurn = !leftTurn;

                        if (leftTurn)
                        {
                            leftIniPos = leftShadow.transform.position.x;
                            //Debug.Log("LEFT TURN: LEFT MOVE STARTS: " + leftIniPos);
                            //animatedRight.GetComponent<Animator>().SetBool("StepUp", true);
                        }
                        else
                        {
                            rightIniPos = rightShadow.transform.position.x;
                            //Debug.Log("RIGHT TURN: RIGHT MOVE STARTS: " + rightIniPos);
                            //animatedLeft.GetComponent<Animator>().SetBool("StepUp", true);
                        }
                    }

                }

                //LOSE GAME ZZZ
                else if (Physics.Raycast(ray, out hit, Mathf.Infinity, lm_floor.value))
                {
                    //Debug.Log("LOOOOOSE: NOOB");
                }
            }
        }
        else
        {
            //Debug.Log("DRAGGING");

            //IT IS HOLDING
            if (Input.GetMouseButton(0))
            {
                Vector3 currentPointer;
                //currentPointer = Input.mousePosition;

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, lm_floor.value))
                {
                    currentPointer = hit.point;

                    float deltaX = (oldPointer - currentPointer).x;

                    if (true)
                    {
                        vel = deltaX / Time.fixedDeltaTime;

                        ((PlatformContentManager)platform.GetComponent("PlatformContentManager")).worldVelocity = -vel;

                        if (leftTurn)
                        {
                            ((ShadowControl)leftShadow.GetComponent("ShadowControl")).vel = vel;
                            ((ShadowControl)rightShadow.GetComponent("ShadowControl")).vel = -vel;
                        }
                        else
                        {
                            ((ShadowControl)rightShadow.GetComponent("ShadowControl")).vel = vel;
                            ((ShadowControl)leftShadow.GetComponent("ShadowControl")).vel = -vel;
                        }

                        //oldPointer = Input.mousePosition;
                        oldPointer = hit.point;

                        // Get animation T

                        float dX = 0;
                        if (leftTurn)
                        {
                            dX = leftShadow.transform.position.x - leftIniPos;
                            if (dX < 0)
                                dX = 0;
                            //animatedLeft.GetComponent<Animator>().SetFloat("UpT", dX / 10);
                            animatedLeft.GetComponent<Animator>().Play("LegStepUp", 0, dX / 10);
                            //Debug.Log("LEFT TURN: LEFT ADVANCE: " + dX);   
                            leftShadow.GetComponent<ShadowControl>().ground = false;
                        }
                        else
                        {
                            dX = rightShadow.transform.position.x - rightIniPos;
                            if (dX < 0)
                                dX = 0;
                            //animatedRight.GetComponent<Animator>().SetFloat("UpT", dX / 10);
                            animatedRight.GetComponent<Animator>().Play("LegStepUp", 0, dX / 10);
                            //Debug.Log("RIGHT TURN: RIGHT ADVANCES: " + dX);
                            rightShadow.GetComponent<ShadowControl>().ground = false;
                        }
                    }
                    else
                    {
                        vel = 0;

                        ((PlatformContentManager)platform.GetComponent("PlatformContentManager")).worldVelocity = 0;

                        ((ShadowControl)leftShadow.GetComponent("ShadowControl")).vel = 0;
                        ((ShadowControl)rightShadow.GetComponent("ShadowControl")).vel = -0;

                    }
                }
            }

            // RELEASE
            if (Input.GetMouseButtonUp(0))
            {


                //vel *= 5f; //scale 


                ////add velocity to floor depending on the swipe
                //((PlatformContentManager)platform.GetComponent("PlatformContentManager")).worldVelocity = -vel;

                //((ShadowControl)rightShadow.GetComponent("ShadowControl")).vel *= 0.1f;
                //((ShadowControl)leftShadow.GetComponent("ShadowControl")).vel *= 0.1f;

                //add velocity to floor depending on the swipe
                ((PlatformContentManager)platform.GetComponent("PlatformContentManager")).worldVelocity = 0;

                ((ShadowControl)rightShadow.GetComponent("ShadowControl")).vel = 0;
                ((ShadowControl)leftShadow.GetComponent("ShadowControl")).vel = 0;


                dragging = false;

                if (leftTurn)
                {
                    animatedLeft.GetComponent<Animator>().Play("LegStepDo");
                    //animatedLeft.GetComponent<Animator>().PlayInFixedTime("LegStepDo", 0, 0.25f);
                    leftShadow.GetComponent<ShadowControl>().ground = true;
                }
                else
                {
                    animatedRight.GetComponent<Animator>().Play("LegStepDo");
                    //animatedRight.GetComponent<Animator>().PlayInFixedTime("LegStepDo", 0, 0.25f);
                    rightShadow.GetComponent<ShadowControl>().ground = true;
                }

                cameraShaking = true;
                originalPos = Camera.main.transform.localPosition;

                // Play dust step particles
                if (leftTurn)
                {
                    if (((CleanStomp)leftClean.GetComponent("CleanStomp")).clean)
                        leftParticles.Play();
                }
                else
                {
                    if (((CleanStomp)rightClean.GetComponent("CleanStomp")).clean)
                        rightParticles.Play();
                }

                gameController.balancing = false;
                gameController.resetBalance();

                this.GetComponent<AudioSource>().PlayOneShot(stompAudio);
            }
        }


        if (cameraShaking)
        {
            shakeCamera();
        }
    }


    // How long the object should shake for.
    

    const float SHAKETIME = 0.3f;

    float shakeDuration = SHAKETIME;

    // Amplitude of the shake. A larger value shakes the camera harder.
    float shakeAmount = 0.5f;
    float decreaseFactor = 1.0f;

    Vector3 originalPos;

    void  shakeCamera()
    {

        if (shakeDuration > 0)
        {
            Camera.main.transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            cameraShaking = false;
            shakeDuration = SHAKETIME;
            Camera.main.transform.localPosition = originalPos;
        }
    }

}
