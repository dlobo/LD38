﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public float currentHealth = 0.0f;
    public float maximumHealth = 1.0f;
    public int currentPoints = 0;

    public float currentBalance = 0.0f;
    public float maximumBalance = 2.5f;

    public float damageVehicleHit = 0.25f;
    public float damageHumanHit = 0.05f;

    public ShadowControl leftShadow = null;
    public ShadowControl rightShadow = null;
    public bool balancing = false;

    // GUI
	public GameObject GameUI;
	public GameObject GameOverMenu;
	public Text gameOverText;
	public Text tipText;
    public Slider balanceSlider = null;
    public Image healthImage = null;
    public Text pointsText = null;
	public Text finalScoreText;
	public Text finalDistanceText;

    public GameObject[] healthSprites;

	// Use this for initialization
	void Start () {
		
        this.restoreHealth(-1);
        this.restoreBalance(-1);

        this.currentPoints = 0;

        this.balanceSlider.maxValue = maximumBalance;
        this.balanceSlider.value = currentBalance;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (balancing)
        {
            this.reduceBalance(Time.deltaTime);
        }

        // Update points UI

        this.pointsText.text = currentPoints.ToString()+"m";

        // Update balance UI

        this.balanceSlider.value = currentBalance;

        // Update health UI

        for (int i = 0; i < this.healthSprites.Length; ++i)
            this.healthSprites[i].SetActive(false);

        if (this.currentHealth <= 0)
            this.healthSprites[0].SetActive(true);
        else if (this.currentHealth <= 0.2)
            this.healthSprites[1].SetActive(true);
        else if (this.currentHealth <= 0.4)
            this.healthSprites[2].SetActive(true);
        else if (this.currentHealth <= 0.6)
            this.healthSprites[3].SetActive(true);
        else if (this.currentHealth <= 0.8)
            this.healthSprites[4].SetActive(true);
        else if (this.currentHealth <= 1.0)
            this.healthSprites[5].SetActive(true);
    }

    public void setPoints(int points)
    {
        this.currentPoints = points;
    }

    public void addPoints(int points)
    {
        this.currentPoints += points;
    }

    public void resetBalance()
    {
        this.currentBalance = this.maximumBalance;
    }

    public void restoreHealth(float health)
    {
        if (health < 0)
            this.currentHealth = this.maximumHealth;
        else
        {
            this.currentHealth += health;
            if (this.currentHealth > this.maximumHealth)
                this.currentHealth = this.maximumHealth;
        }

        //Debug.Log("CURRENT HEALTH: " + this.currentHealth);
    }

    public void restoreBalance(float balance)
    {
        if (balance < 0)
            this.currentBalance = this.maximumBalance;
        else
        {
            this.currentBalance += balance;
            if (this.currentBalance > this.maximumBalance)
                this.currentBalance = this.maximumBalance;
        }

        //Debug.Log("CURRENT BALANCE: " + this.currentBalance);
    }

    public void reduceHealth(float health)
    {
        this.currentHealth -= health;
        if (this.currentHealth <= 0)
            this.gameOver(0);

        //Debug.Log("CURRENT HEALTH: " + this.currentHealth);
    }

    public void reduceBalance(float balance)
    {
        this.currentBalance -= balance;
        if (this.currentBalance <= 0)
            this.gameOver(1);

        //Debug.Log("CURRENT BALANCE: " + this.currentBalance);
    }

    public void gameOver(int cause)
    {
		GameUI.SetActive (false);
		GameOverMenu.SetActive (true);

		if (cause == 0)
        {
			gameOverText.text = "YOU GOT \n DEPRESSED";
			tipText.text = "Try to walk safer!";
			finalDistanceText.text = currentPoints.ToString()+" m";
			finalScoreText.text = (currentPoints * 1.0f).ToString();

			// TODO: Game over no health
        }

        if (cause == 1)
        {
            // TODO: Game over no balance
			gameOverText.text = "YOU LOST \n BALANCE";
			tipText.text = "Try to walk faster!";

			finalDistanceText.text = currentPoints.ToString()+" m";
			if (this.currentHealth >= 1) 
			{
				finalScoreText.text = (currentPoints * 2.0f).ToString();
			} else if (this.currentHealth >= 0.75f && this.currentHealth <= 1.0f) 
			{
				finalScoreText.text = (currentPoints * 1.75f).ToString();
			} 
			else if (this.currentHealth >= 0.5f && this.currentHealth <= 0.75f) 
			{
				finalScoreText.text = (currentPoints * 1.5f).ToString();
			} 
			else if (this.currentHealth >= 0.25f && this.currentHealth <= 0.5f) 
			{
				finalScoreText.text = (currentPoints * 1.25f).ToString();
			} 
			else if (this.currentHealth >= 0.0f && this.currentHealth <= 0.25f) 
			{
				finalScoreText.text = (currentPoints * 1.15f).ToString();
			}
        }

        Destroy(this.leftShadow.gameObject);
        Destroy(this.rightShadow.gameObject);
    }
    


}
