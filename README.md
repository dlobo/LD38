# ⚠⚠⚠⚠⚠ DANGER : JAM CODE AHEAD : BE CAREFUL ⚠⚠⚠⚠⚠ #

This is the repository of our game [Crazy People, Friendly Giant](https://ldjam.com/events/ludum-dare/38/crazy-people-friendly-giant) in 38th Ludum Dare. Have fun! 

## Introduction ##

It’s hard to be a friendly giant in this crazy world. People panic upon seeing you. They seem to love chilling under your feet and crashing their vehicles into your legs; everyone’s screaming and dying… And that makes a giant sad; giants love going out for a walk! 😢

![friendlyG.png](https://bitbucket.org/repo/kMMxMR7/images/3048368611-friendlyG.png)

## Game instructions  ##

Help the giant walk as far as possible before losing balance or killing too many people.

This is not just another endless runner.

It should be played on a mobile device (but can also be playtested on Windows).

Place you device horizontally in a steady surface.
Put your index and middle fingers on top of the giant’s feet.
Alternatively touch and drag backwards the feet of the giant simulating a treadmill.
The shadow of the giant will show you where the other foot will be placed in the next step.
Avoid stepping on people and try to dodge vehicles crashing into your legs.
If you take too long to make the step, you’ll lose balance.
Example:

```
#!
    - Touch LEFT foot, drag backwards to define RIGHT foot step position, release LEFT foot.
    - Touch RIGHT foot, drag backward to define LEFT foot step position, release RIGHT foot.
    - Touch LEFT foot, ...
```

![1a31.png](https://bitbucket.org/repo/kMMxMR7/images/229829457-1a31.png)

Have fun!

P.S: If you are playing on Windows, simulate touch&drag using mouse click and movement.

## Download ##

[Windows (EXE)](https://drive.google.com/open?id=0B6lIlNc6C9fCNFNPaWdqd1F4OUU)

[Android (APK)](https://drive.google.com/open?id=0B6lIlNc6C9fCd05maHB0X2lTek0)

## Authors ##

@JesusProd
@dlobo
@RMart
@dorre